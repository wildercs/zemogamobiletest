//
//  HomeViewController.swift
//  ZemogaApp
//
//  Created by Wilder on 26/05/22.
//

import UIKit

class HomeViewController: UIViewController, Alertable {
    
    @IBOutlet weak var tableView: UITableView!
    private var homePresenter: HomePresenter!
    
    var posts: [PostItem] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        loadUI()
        homePresenter = HomePresenter(view: self)
        refresh()
    }
    
    private func loadUI() {
        configureNavigationBar(
            largeTitleColor: .white,
            backgoundColor: .green,
            tintColor: .white,
            title: "Posts",
            preferredLargeTitle: false
        )
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .refresh,
            target: self,
            action: #selector(refresh)
        )
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @objc func refresh() {
        self.homePresenter.fetchPosts()
    }
}

// MARK: - UITableViewDataSource
extension HomeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "postTableViewCell", for: indexPath as IndexPath) as! PostTableViewCell
        let post = self.posts[indexPath.row]
        cell.loadData(post: post)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = self.posts[indexPath.row]
        let detailVC = DetailPostViewController.create(withPost: post)
        self.show(detailVC, sender: nil)
    }
}

// MARK: - HomePresenter
extension HomeViewController: HomeView {
    
    func showPosts(posts: [PostItem]) {
        self.posts = posts
        self.tableView.reloadData()
    }
    
    func showError(message: String) {
        showAlert(message: message)
    }
    
    func showLoading() {
        LoadingView.show()
    }
    
    func dismissLoading() {
        LoadingView.hide()
    }
}
