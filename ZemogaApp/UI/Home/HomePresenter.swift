//
//  HomePresenter.swift
//  ZemogaApp
//
//  Created by Wilder on 27/05/22.
//

import Foundation

protocol HomeView {
    func showPosts(posts: [PostItem])
    func showError(message: String)
    func showLoading()
    func dismissLoading()
}

class HomePresenter: NSObject {
    
    private var view: HomeView!
    private var dataManager: MainDataSourceProtocol!
    
    init(view: HomeView) {
        self.view = view
        self.dataManager = DataSourceManager()
    }
    
    func fetchPosts() {
        view.showLoading()
        dataManager.getPosts(callbackHandler: { [weak self](posts: [PostItem], error) in
            if error == nil {
                self?.view.showPosts(posts: posts)
            } else {
                self?.view.showError(message: "Server Error")
            }
            self?.view.dismissLoading()
        })
    }
}
