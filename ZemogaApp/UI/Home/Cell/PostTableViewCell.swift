//
//  PostTableViewCell.swift
//  ZemogaApp
//
//  Created by Wilder on 26/05/22.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func loadData(post: PostItem) {
        titleLabel.text = post.title
    }

}
