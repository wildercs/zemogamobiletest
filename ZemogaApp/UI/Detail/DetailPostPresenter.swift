//
//  DetailPostPresenter.swift
//  ZemogaApp
//
//  Created by Wilder on 27/05/22.
//

import Foundation

protocol DetailPostView {
    func showUser(user: UserItem)
    func showComments(comments: [CommentItem])
    func showError(message: String)
    func showLoading()
    func dismissLoading()
}

class DetailPostPresenter: NSObject {
    
    private var view: DetailPostView!
    private var dataManager: MainDataSourceProtocol!
    
    init(view: DetailPostView) {
        self.view = view
        self.dataManager = DataSourceManager()
    }
    
    func fetchUser(postId: String, userdId: String) {
        view.showLoading()
        dataManager.getUser(withId: userdId) { [weak self](users: [UserItem], error) in
            self?.view.dismissLoading()
            if error == nil {
                if users.count > 0 {
                    self?.view.showUser(user: users[0])
                } else {
                    self?.view.showError(message: "User not found")
                }
            } else {
                self?.view.showError(message: "Server Error")
            }
            self?.getComments(postId: postId)
        }
    }
    
    private func getComments(postId: String) {
        view.showLoading()
        dataManager.getComments(withId: postId) { [weak self](comments: [CommentItem], error) in
            if error == nil {
                self?.view.showComments(comments: comments)
            } else {
                self?.view.showError(message: "Comments not found")
            }
            self?.view.dismissLoading()
        }
    }
}
