//
//  DetailPostViewController.swift
//  ZemogaApp
//
//  Created by Wilder on 26/05/22.
//

import UIKit

class DetailPostViewController: UIViewController {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var userPhoneLabel: UILabel!
    @IBOutlet weak var userWebsiteLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var post: PostItem?
    var comments: [CommentItem] = []
    var detailPostPresenter: DetailPostPresenter!
    
    static func create(withPost post: PostItem) -> DetailPostViewController {
        let vc = DetailPostViewController.instantiateViewController()
        vc.post = post
        vc.modalPresentationStyle = .fullScreen
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        loadUI()
    }
    
    private func loadUI() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "favorite"),
            style: .plain,
            target: self,
            action: #selector(favoriteAction)
        )
        setupTitle()
        detailPostPresenter = DetailPostPresenter(view: self)
        
        if let post = self.post {
            descriptionLabel.text = post.body
            detailPostPresenter.fetchUser(postId: String(post.id ?? 0), userdId: String(post.userId ?? 0))
        }
    }
    
    @objc func favoriteAction() {
        print("favoriteAction.")
    }
    
    private func setupTitle() {
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: 50, height: 40))
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.text = "Post"
        label.numberOfLines = 1
        label.textColor = .white
        label.sizeToFit()
        label.textAlignment = .center
        self.navigationItem.titleView = label
    }
    
    private func updateUserView(user: UserItem) {
        userNameLabel.text = user.name
        userEmailLabel.text = user.email
        userPhoneLabel.text = user.phone
        userWebsiteLabel.text = user.website
    }
}

// MARK: - StoryboardInstantiable, Alertable
extension DetailPostViewController: StoryboardInstantiable, Alertable {
    static var defaultFileName: String { return "DetailPost" }
}

// MARK: - UITableViewDataSource
extension DetailPostViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }
    
    /*func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Comments"
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentTableViewCell", for: indexPath as IndexPath) as! CommentTableViewCell
        let comment = self.comments[indexPath.row]
        cell.loadData(text: comment.body ?? "")
        return cell
    }
}

// MARK: - UITableViewDelegate
extension DetailPostViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 30))
            returnedView.backgroundColor = .lightGray

            let label = UILabel(frame: CGRect(x: 10, y: 5, width: view.frame.size.width, height: 20))
            label.text = "Comments"
            label.textColor = .black
            returnedView.addSubview(label)

            return returnedView
    }
}

extension DetailPostViewController: DetailPostView {
    
    func showUser(user: UserItem) {
        updateUserView(user: user)
    }
    
    func showComments(comments: [CommentItem]) {
        self.comments = comments
        self.tableView.reloadData()
    }
    
    func showError(message: String) {
        showAlert(message: message)
    }
    
    func showLoading() {
        LoadingView.show()
    }
    
    func dismissLoading() {
        LoadingView.hide()
    }
}
