//
//  CommentTableViewCell.swift
//  ZemogaApp
//
//  Created by Wilder on 27/05/22.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var commentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func loadData(text: String) {
        commentLabel.text = text
    }

}
