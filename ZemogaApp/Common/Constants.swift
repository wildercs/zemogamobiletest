//
//  Constants.swift
//  ZemogaApp
//
//  Created by Wilder on 26/05/22.
//

import Foundation

struct Constants {
    static let baseURL = URL(string: "https://jsonplaceholder.typicode.com/")
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
    case string = "String"
}

enum ContentType: String {
    case json = "Application/json"
    case text = "text/plain;charset=UTF-8"
    case formEncode = "application/x-www-form-urlencoded"
}
