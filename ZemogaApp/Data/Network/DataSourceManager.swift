//
//  DataSourceManager.swift
//  ZemogaApp
//
//  Created by Wilder on 26/05/22.
//

import Foundation
import Alamofire

typealias postsResponse = ([PostItem], Error?) -> Void
typealias commentsResponse = ([CommentItem], Error?) -> Void
typealias userResponse = ([UserItem], Error?) -> Void

protocol MainDataSourceProtocol {
    func getPosts(callbackHandler: @escaping postsResponse)
    func getComments(withId postId: String, callbackHandler: @escaping commentsResponse)
    func getUser(withId userId: String, callbackHandler: @escaping userResponse)
}

class DataSourceManager: MainDataSourceProtocol {
    
    func getPosts(callbackHandler: @escaping postsResponse) {
        AF.request(APIRouter.getPosts)
            .responseDecodable { (response: DataResponse<[PostItem], AFError>) in
                switch response.result {
                case .success(let result):
                    callbackHandler(result, nil)
                case .failure(let error):
                    callbackHandler([], error)
                }
            }
    }
    
    func getComments(withId postId: String, callbackHandler: @escaping commentsResponse) {
        AF.request(APIRouter.getComments(postId: postId))
            .responseDecodable{ (response: DataResponse<[CommentItem], AFError>) in
                switch response.result {
                case .success(let result):
                    callbackHandler(result, nil)
                case .failure(let error):
                    callbackHandler([], error)
                }
            }
    }
    
    func getUser(withId userId: String, callbackHandler: @escaping userResponse) {
        AF.request(APIRouter.getUser(userId: userId))
            .responseDecodable{ (response: DataResponse<[UserItem], AFError>) in
                switch response.result {
                case .success(let result):
                    callbackHandler(result, nil)
                case .failure(let error):
                    callbackHandler([], error)
                }
            }
    }
}
