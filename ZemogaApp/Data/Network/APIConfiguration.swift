//
//  APIConfiguration.swift
//  ZemogaApp
//
//  Created by Wilder on 26/05/22.
//

import Foundation
import Alamofire

protocol APIConfiguration: URLRequestConvertible {
    var baseURL: URL { get }
    var requestURL: URL { get }
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: RequestParams? { get }
    var headers: HTTPHeaders { get }
}
