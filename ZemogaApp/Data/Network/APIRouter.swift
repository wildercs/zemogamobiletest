//
//  APIRouter.swift
//  ZemogaApp
//
//  Created by Wilder on 26/05/22.
//

import Foundation
import Alamofire

enum RequestParams {
    case bodyWhitData(_: Data)
    case body(_: Parameters)
    case url(_: Parameters)
}

enum APIRouter: APIConfiguration {
    
    case getPosts
    case getComments(postId: String)
    case getUser(userId: String)
    
    // MARK: - baseURL
    var baseURL: URL {
        return Constants.baseURL!
    }
    
    // MARK: - requestURL
    var requestURL: URL {
        return baseURL.appendingPathComponent(path, isDirectory: false)
    }
    
    // MARK: - HTTPMethod
    var method: HTTPMethod {
        return .get
    }
    // MARK: - Parameters
     var parameters: RequestParams? {
        switch self {
        case .getPosts:
            return .none
        case .getComments(let postId):
            return .url(["postId": postId])
        case .getUser(let userId):
            return .url(["id" : userId])
        }
    }
    
    // MARK: - Path
    var path: String {
        switch self {
        case .getPosts:
            return "posts"
        case .getComments:
            return "comments"
        case .getUser:
            return "users"
        }
    }
    
    // MARK: - HTTPHeaders
    var headers: HTTPHeaders {
        var headers = HTTPHeaders()
        switch self {
        case .getPosts, .getComments, .getUser:
            headers.add(name: HTTPHeaderField.contentType.rawValue, value: ContentType.text.rawValue)
            return headers
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: requestURL)
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.addValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        headers.forEach {
            urlRequest.addValue($0.value, forHTTPHeaderField: $0.name)
        }
        
        // Parameters
        switch parameters {
        case .bodyWhitData(let data):
            urlRequest.httpBody = data
        case .body(let params):
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
        case .url(let params):
                let queryParams = params.map { pair  in
                    return URLQueryItem(name: pair.key, value: "\(pair.value)")
                }
                var components = URLComponents(string: requestURL.absoluteString)
                components?.queryItems = queryParams
                urlRequest.url = components?.url
        case .none: break
        }
        return urlRequest
    }
}
