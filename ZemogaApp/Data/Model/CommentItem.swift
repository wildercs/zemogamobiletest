//
//  CommentItem.swift
//  ZemogaApp
//
//  Created by Wilder on 26/05/22.
//

import Foundation

struct CommentItem : Codable {
    let postId: Int?
    let id: Int?
    let name: String?
    let email: String?
    let body: String?

    enum CodingKeys: String, CodingKey {
        case postId = "postId"
        case id = "id"
        case name = "name"
        case email = "email"
        case body = "body"
    }
}
