//
//  UserItem.swift
//  ZemogaApp
//
//  Created by Wilder on 27/05/22.
//

import Foundation

struct UserItem : Codable {
    let id: Int?
    let name: String?
    let userName: String?
    let email: String?
    let address: AddressItem?
    let phone: String?
    let website: String?
    let company: CompanyItem?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case userName = "username"
        case email = "email"
        case address = "address"
        case phone = "phone"
        case website = "website"
        case company = "company"
    }
}

struct GeoItem: Codable {
    let lat: String?
    let lng: String?

    enum CodingKeys: String, CodingKey {
        case lat = "lat"
        case lng = "lng"
    }
}

struct AddressItem: Codable {
    let street: String?
    let suite: String?
    let city: String?
    let zipcode: String?
    let geo: GeoItem?

    enum CodingKeys: String, CodingKey {
        case street = "street"
        case suite = "suite"
        case city = "city"
        case zipcode = "zipcode"
        case geo = "geo"
    }
}

struct CompanyItem: Codable {
    let name: String?
    let catchPhrase: String?
    let bs: String?

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case catchPhrase = "catchPhrase"
        case bs = "bs"
    }
}
