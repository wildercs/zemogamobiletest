//
//  PostItem.swift
//  ZemogaApp
//
//  Created by Wilder on 26/05/22.
//

import Foundation
struct PostItem: Codable {
    let userId: Int?
    let id: Int?
    let title: String?
    let body: String?

    enum CodingKeys: String, CodingKey {
        case userId = "userId"
        case id = "id"
        case title = "title"
        case body = "body"
    }
}
